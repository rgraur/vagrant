# -*- mode: ruby -*-
# vi: set ft=ruby :

# General configuration
project_name = "project-name.local"
box_name     = "debian_7_7_0_amd64_" + project_name
box_url      = "https://bitbucket.org/rgraur/vagrant/downloads/debian_7_7_0_amd64.box"

# Server configuration
hostname        = project_name
server_ip       = "192.168.2.2"
server_cpus     = "1"
server_memory   = "512" # MB
server_swap     = "768" # MB or false
server_timezone = "UTC" # UTC, EST, US/Central, US/Eastern

# Apache configuration
apache_port          = 80
apache_document_root = "/var/www/"

# MySQL configuration
mysql_root_password = "vagrant"

# PHP configuration
php_timezone    = "UTC" # http://php.net/manual/en/timezones.php
php_xdebug_port = 9000
php_xdebug_key  = project_name

# Project configuration
project_public_path  = apache_document_root + "public/"
project_db_user      = "projectdbuser"
project_db_password  = "projectdbpass"
project_db_name      = "projectdbname"

################################################################################

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = box_name

  # Set private key
  config.ssh.private_key_path = "./keys/debian_7_7_0_amd64/id_rsa"

  # Avoid "stdin: is not a tty" errors
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

  # Box URL
  config.vm.box_url = box_url

  # Configure hostmanager plugin if installed
  if Vagrant.has_plugin?("vagrant-hostmanager")
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true
    config.hostmanager.aliases = "project-alias"
    config.vm.provision :hostmanager
  end

  # Create hostname
  config.vm.hostname = hostname

  # Create static IP
  config.vm.network :private_network, ip: server_ip

  # Share folders
  config.vm.synced_folder "../", apache_document_root,
    owner: "vagrant",
    group: "www-data",
    :mount_options => ["dmode=775"]

  config.vm.synced_folder "../app/storage" , apache_document_root + "/app/storage",
    owner: "vagrant",
    group: "www-data",
    :mount_options => ["dmode=775", "fmode=664"]

  # Configure VirtualBox
  config.vm.provider :virtualbox do |vb|
    vb.name = hostname
    vb.gui  = false

    # Set CPUs and memory
    vb.customize ["modifyvm", :id, "--cpus", server_cpus]
    vb.customize ["modifyvm", :id, "--memory", server_memory]

    # Set the timesync threshold to 10 seconds, instead of the default 20 minutes.
    # If the clock gets more than 15 minutes out of sync (due to your laptop going
    # to sleep for instance, then some 3rd party services will reject requests.
    vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]
  end

  ##############################################################################

  # Provisioners (uncomment to provision)

  # Base
  config.vm.provision "shell", path: "provisioners/base.sh", args: [server_swap, server_timezone]

  # PHP
  config.vm.provision "shell", path: "provisioners/php.sh", args: [php_timezone, php_xdebug_port, php_xdebug_key]

  # Apache
  config.vm.provision "shell", path: "provisioners/apache.sh", args: [hostname, apache_port, apache_document_root]

  # MySQL
  config.vm.provision "shell", path: "provisioners/mysql.sh", args: [mysql_root_password]

  # Composer
  config.vm.provision "shell", path: "provisioners/composer.sh"

  # NodeJS
  config.vm.provision "shell", path: "provisioners/nodejs.sh"

  # Grunt
  config.vm.provision "shell", path: "provisioners/grunt.sh"

  # Redis
  config.vm.provision "shell", path: "provisioners/redis.sh"

  # ElasticSearch (depends on OpenJDK)
  config.vm.provision "shell", path: "provisioners/elasticsearch.sh"

  # Deploy application
  config.vm.provision "shell", path: "provisioners/deploy.sh", args: [apache_document_root, hostname, mysql_root_password, project_db_user, project_db_password, project_db_name, project_public_path]

  # Customize with personal packages and settings
  config.vm.provision "shell", path: "provisioners/customize.sh"
end
