#!/usr/bin/env bash

# Install and configure PHP5.5
#
# arg $1 php_timezone
# arg $2 php_xdebug_port
# arg $3 php_xdebug_key

echo ">>> Installing PHP5..."
export LANG=C.UTF-8
PHP_TIMEZONE=$1
touch /etc/apt/sources.list.d/php_5_5.list
cat <<- EOF > /etc/apt/sources.list.d/php_5_5.list
  deb http://packages.dotdeb.org wheezy-php55 all
  deb-src http://packages.dotdeb.org wheezy-php55 all
EOF
cd /tmp
wget http://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg
sudo apt-key update
sudo apt-get update
sudo apt-get install -qq php5 php5-cli php5-mysql php5-curl php5-mcrypt php5-xdebug

echo ">>> Configuring XDEBUG..."
cat <<- EOF > /etc/php5/mods-available/xdebug.ini
  xdebug.remote_enable = 1
  xdebug.remote_connect_back = 1
  xdebug.remote_port = $2
  xdebug.remote_mode = "req"
  xdebug.remote_handler = "dbgp"
  xdebug.idekey = "$3"
EOF

