#!/usr/bin/env bash

# Install and configure Apache2
#
# arg $1 hostname
# arg $2 apache_port
# arg $3 apache_document_root

echo ">>> Installing Apache2..."
sudo apt-get install -qq apache2 apache2-mpm-event libapache2-mod-php5

echo ">>> Configuring Apache2..."

# add vagrant user to www-data group
sudo usermod -a -G www-data vagrant

# enable php5 if installed
sudo a2enmod php5
sudo a2enmod rewrite

# delete default virtual host and page
rm /etc/apache2/sites-enabled/000-default
rm /var/www/index.html

# add virtual host
cat <<- EOF > "/etc/apache2/sites-available/$1"
  <VirtualHost *:$2>
    ServerAdmin webmaster@localhost
    ServerName $1
    DocumentRoot $3

    <Directory />
      Options FollowSymLinks
      AllowOverride None
    </Directory>
    <Directory $3>
      Options Indexes FollowSymLinks MultiViews
      AllowOverride All
      Order allow,deny
      allow from all
    </Directory>

    ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
    <Directory "/usr/lib/cgi-bin">
      AllowOverride None
      Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
      Order allow,deny
      Allow from all
    </Directory>

    ErrorLog \${APACHE_LOG_DIR}/error.log

    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn

    CustomLog \${APACHE_LOG_DIR}/access.log combined
  </VirtualHost>
EOF
sudo a2ensite $1

# change port
sed -i "s/80/$2/g" /etc/apache2/ports.conf

# restart server
sudo service apache2 restart

