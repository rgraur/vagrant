#!/usr/bin/env bash

# Change base system settings and install base packages
#
# arg $1 server_swap
# arg $2 server_timezone

# Set timezone
echo ">>> Setting Timezone & Locale to $2 & C.UTF-8..."
sudo ln -sf /usr/share/zoneinfo/$2 /etc/localtime
sudo locale-gen C.UTF-8
export LANG=C.UTF-8
echo "export LANG=C.UTF-8" >> /home/vagrant/.bashrc

# Install base packages
echo ">>> Installing base packages..."
sudo apt-get install -qq curl unzip git-core

