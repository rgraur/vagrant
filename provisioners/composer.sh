#!/usr/bin/env bash

# Install and configure Composer

echo ">>> Installing Composer..."
cd /tmp
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

