#!/usr/bin/env bash

# Deploy application
#
# arg $1 apache_document_root
# arg $2 hostname
# arg $3 mysql_root_password
# arg $4 project_db_user
# arg $5 project_db_password
# arg $6 project_db_name
# arg $7 project_public_path

echo ">>> Deploy application..."
cd $1

# Fill with project customizations

