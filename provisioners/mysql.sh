#!/usr/bin/env bash

# Install and configure MySQL
#
# arg $1 mysql_root_password

echo ">>> Installing MySQL..."
debconf-set-selections <<< "mysql-server mysql-server/root_password password $1"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $1"
sudo apt-get install -qq mysql-server mysql-client

