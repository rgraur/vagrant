#!/usr/bin/env bash

# Install and configure NodeJS

echo ">>> Installing NodeJS..."
cd /tmp
curl -sL https://deb.nodesource.com/setup | bash -
sudo apt-get install -qq nodejs
