#!/usr/bin/env bash

# Install and configure Redis

echo ">>> Installing Redis..."
cd /tmp
wget http://download.redis.io/releases/redis-2.8.9.tar.gz
tar xzf redis-2.8.9.tar.gz
cd redis-2.8.9
make
sudo make install
cd utils
./install_server.sh # TODO: find a way to ask to all questions

