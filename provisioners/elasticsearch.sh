#!/usr/bin/env bash

# Install and configure ElasticSearch

echo ">>> Installing ElasticSearch"
sudo apt-get install -qq openjdk-7-jre
cd /tmp
wget -qO - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
touch /etc/apt/sources.list.d/elasticsearch.list
cat <<- EOF > /etc/apt/sources.list.d/elasticsearch.list
  deb http://packages.elasticsearch.org/elasticsearch/1.4/debian stable main
EOF
sudo apt-get update
sudo apt-get install -qq elasticsearch
service elasticsearch start

