# Vagrant generic provisioning scripts #

The project contains a generic Vagrantfile and scripts to provision with LAMP packages.

### Install ###
* Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* Install [Vagrant](https://www.vagrantup.com/downloads.html)
* Install [vagrant-hostmanager](https://github.com/smdahlen/vagrant-hostmanager) plugin
```
vagrant plugin install vagrant-hostmanager
```
* Clone repo to your project root
```
git clone git@bitbucket.org:rgraur/vagrant.git /path/to/your/project/vagrant
```
* Add `vagrant` folder to `.gitignore` on your project
```
echo "vagrant" >> .gitignore
```
* Edit `Vagrantfile` configuration to match your project settings
```
project_name, apache_port, project_db_user, project_db_password, project_db_name
```
* Create vagrant box using instructions from [Wiki](https://bitbucket.org/rgraur/vagrant/wiki/Create%20Vagrant%20Box) or use an existent box from [Downloads](https://bitbucket.org/rgraur/vagrant/downloads)
* Add project deployment procedures to `provisioners/deploy.sh`
* Add personal customizations to `provisioners/customize.sh`
* Instantiate server
```
vagrant up
```

### Default packages ###
* cURL
* unzip
* GIT
* PHP 5.5
* Apache
* MySQL
* Composer
* NodeJS
* Grunt
* Redis
* ElasticSearch

### Use ###
* Open project in browser using `hostname` from `Vagrantfile`
* Connect to server: `vagrant ssh`
* Use private key from `keys` folder to connect to MySQL over SSH